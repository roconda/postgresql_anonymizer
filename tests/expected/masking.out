CREATE EXTENSION IF NOT EXISTS anon CASCADE;
-- INIT
SELECT anon.mask_init();
 mask_init 
-----------
 t
(1 row)

CREATE TABLE t1 (
	id SERIAL,
	name TEXT,
	"CreditCard" TEXT,
	fk_company INTEGER
);
INSERT INTO t1 
VALUES (1,'Schwarzenegger','1234567812345678', 1991);
COMMENT ON COLUMN t1.name IS '  MASKED WITH FUNCTION anon.random_last_name() ';
COMMENT ON COLUMN t1."CreditCard" IS '  MASKED    WITH    FUNCTION         anon.random_string(12)';
CREATE TABLE "T2" (
	id_company SERIAL,
	"IBAN" TEXT,
	COMPANY TEXT
);
INSERT INTO "T2"
VALUES (1991,'12345677890','Cyberdyne Systems');
COMMENT ON COLUMN "T2"."IBAN" IS 'MASKED WITH FUNCTION anon.random_iban()';
COMMENT ON COLUMN "T2".COMPANY IS 'jvnosdfnvsjdnvfskngvknfvg MASKED WITH FUNCTION anon.random_company() jenfk snvi  jdnvkjsnvsndvjs';
SELECT count(*) = 4  FROM anon.pg_masks;
 ?column? 
----------
 t
(1 row)

SELECT masking_function = 'anon.random_iban()' FROM anon.pg_masks WHERE attname = 'IBAN';
 ?column? 
----------
 t
(1 row)

-- 
SELECT company != 'Cyberdyne Systems' FROM mask."T2" WHERE id_company=1991;
 ?column? 
----------
 t
(1 row)

SELECT name != 'Schwarzenegger' FROM mask.t1 WHERE id = 1;
 ?column? 
----------
 t
(1 row)

-- ROLE
CREATE ROLE skynet LOGIN;
COMMENT ON ROLE skynet IS 'MASKED';
-- FORCE update because COMMENT doesn't trigger the Event Trigger
SELECT anon.mask_update();
 mask_update 
-------------
(0 rows)

SELECT anon.hasmask('skynet');
 hasmask 
---------
 t
(1 row)

SELECT anon.hasmask('postgres') IS FALSE;
 ?column? 
----------
 t
(1 row)

SELECT anon.hasmask(NULL) IS NULL; 
 ?column? 
----------
 t
(1 row)

\! psql contrib_regression -U skynet -c 'SHOW search_path;'
 search_path  
--------------
 mask, public
(1 row)

-- Disabling this test, because the error message has changed between PG10 and PG11 
-- This test should fail anyway, the skynet role is not allowed to access the t1 table
--\! psql contrib_regression -U skynet -c "SELECT * FROM public.t1;"
\! psql contrib_regression -U skynet -c "SELECT name != 'Schwarzenegger' FROM t1 WHERE id = 1;"
 ?column? 
----------
 t
(1 row)

\! psql contrib_regression -U skynet -c "SELECT company != 'Cyberdyne Systems' FROM \"T2\" WHERE id_company=1991;"
 ?column? 
----------
 t
(1 row)

-- STATIC SUBST
SELECT anon.static_substitution();
 static_substitution 
---------------------
 t
(1 row)

SELECT company != 'Cyberdyne Systems' FROM "T2" WHERE id_company=1991;
 ?column? 
----------
 t
(1 row)

SELECT name != 'Schwarzenegger' FROM t1 WHERE id = 1;
 ?column? 
----------
 t
(1 row)

--  CLEAN
DROP EXTENSION anon CASCADE;
NOTICE:  drop cascades to 3 other objects
DETAIL:  drop cascades to view mask.t1
drop cascades to view mask."T2"
drop cascades to event trigger anon_mask_update
REASSIGN OWNED BY skynet TO postgres;
DROP OWNED BY skynet CASCADE;
DROP ROLE skynet;
DROP SCHEMA mask CASCADE;
DROP TABLE "T2";
DROP TABLE t1;
